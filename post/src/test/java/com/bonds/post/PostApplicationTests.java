package com.bonds.post;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
@Disabled("Tests are not configured.")
class PostApplicationTests {

    @Test
    void contextLoads() {
    }

}
