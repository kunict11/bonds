package com.bonds.post.repository;

import com.bonds.post.model.Post;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PostRepository extends CrudRepository<Post, Long> {
    List<Post> findAll();

    @Modifying
    @Query(
            value = "update post set likes = array_append(likes, ?1)",
            nativeQuery = true)
    void appendLike(long userId);
}
