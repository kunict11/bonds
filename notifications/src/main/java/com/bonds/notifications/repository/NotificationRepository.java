package com.bonds.notifications.repository;

import com.bonds.notifications.model.Notification;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;

import java.util.List;

public interface NotificationRepository extends ReactiveMongoRepository<Notification, ObjectId> {
    List<Notification> getById(ObjectId id);
}
