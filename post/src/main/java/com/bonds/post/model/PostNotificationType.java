package com.bonds.post.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum PostNotificationType {
    @JsonProperty("like")
    LIKE,
    @JsonProperty("comment")
    COMMENT
}
