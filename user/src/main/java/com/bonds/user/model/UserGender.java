package com.bonds.user.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum UserGender {
    @JsonProperty("male")
    MALE,
    @JsonProperty("female")
    FEMALE,
    @JsonProperty("other")
    OTHER;
}
