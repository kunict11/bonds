package com.bonds.user.exception;

import com.bonds.common.exception.RestErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class UserExceptionHandler {
    Logger logger = LoggerFactory.getLogger(UserExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleUserNotFoundException(UserNotFoundException e) {
        RestErrorResponse response = new RestErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                System.currentTimeMillis()
        );

        logger.error("Error processing request: {}. Encountered exception: {}", HttpStatus.NOT_FOUND, e.getMessage(), e);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }
}
