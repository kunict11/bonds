package com.bonds.auth.service;

import com.bonds.auth.dto.NewUserRequestDTO;
import com.bonds.auth.dto.RegisterRequestDTO;
import com.bonds.auth.exception.InternalServerErrorException;
import com.bonds.auth.exception.UsernameAlreadyTakenException;
import com.bonds.auth.model.UserAuth;
import com.bonds.auth.repository.UserAuthRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClient;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@Service
public class UserAuthServiceImpl implements UserAuthService {

    @Value("${external.services.user}")
    private String userServiceBaseUrl;
    private final UserAuthRepository userAuthRepository;
    private final PasswordEncoder passwordEncoder;

    public UserAuthServiceImpl(UserAuthRepository userAuthRepository, PasswordEncoder passwordEncoder) {
        this.userAuthRepository = userAuthRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserAuth loadUserByUsername(String username) throws UsernameNotFoundException {
        return userAuthRepository
                .findUserAuthByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User with username " + " not found."));
    }

    @Override
    public UserAuth registerNewUser(RegisterRequestDTO registerRequestDTO, MultipartFile profilePicture)
            throws UsernameAlreadyTakenException, InternalServerErrorException {
        if (userAuthRepository.existsUserAuthByUsername(registerRequestDTO.username()))
            throw new UsernameAlreadyTakenException("Username " + registerRequestDTO.username() + " is already taken.");

        MultiValueMap<String, Object> multipartRequestBody = new LinkedMultiValueMap<>();
        HttpHeaders partHeaders = new HttpHeaders();
        partHeaders.setContentType(MediaType.APPLICATION_JSON);
        NewUserRequestDTO newUserRequestDTO = new NewUserRequestDTO(
                registerRequestDTO.username(),
                registerRequestDTO.alias(),
                registerRequestDTO.bio(),
                registerRequestDTO.gender(),
                registerRequestDTO.country()
        );

        if (profilePicture != null)
            multipartRequestBody.add("profilePicture", profilePicture.getResource());
        multipartRequestBody.add("newUserDTO", new HttpEntity<>(newUserRequestDTO, partHeaders));

        RestClient restClient = RestClient.create();
        ResponseEntity<String> response = restClient.post()
                .uri(userServiceBaseUrl + "/")
                .contentType(MediaType.MULTIPART_FORM_DATA)
                .body(multipartRequestBody)
                .retrieve().toEntity(String.class);

        if (response.getStatusCode() != HttpStatus.CREATED || response.getBody() == null) {
            throw new InternalServerErrorException("An error occurred during registration, please try again later.");
        }

        String userId = response.getBody();

        UserAuth userAuth = new UserAuth();

        userAuth.setEmail(registerRequestDTO.email());
        userAuth.setUsername(registerRequestDTO.username());
        userAuth.setUserId(userId);
        userAuth.setPassword(passwordEncoder.encode(registerRequestDTO.password()));
        userAuth.setEnabled(true);

        return userAuthRepository.save(userAuth);
    }

//    @Override
//    public UserAuth login(LoginRequestDTO loginRequestDTO) throws BadCredentialsException {
//        authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(loginRequestDTO.username(), loginRequestDTO.password()));
//
//        return loadUserByUsername(loginRequestDTO.username());
//    }

    @Override
    public void deleteUserAuthByUsername(String username) {
        UserAuth userAuth = loadUserByUsername(username);

        userAuthRepository.delete(userAuth);
    }
}
