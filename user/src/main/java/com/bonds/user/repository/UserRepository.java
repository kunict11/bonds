package com.bonds.user.repository;

import com.bonds.user.model.User;
import org.springframework.data.neo4j.repository.Neo4jRepository;
import org.springframework.data.neo4j.repository.query.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends Neo4jRepository<User, String> {
    List<User> findAll();

    @Query("MATCH (u1:User)-[:FOLLOWING]->(u2:User) WHERE u2.id = $id RETURN u1")
    List<User> findAllFollowersForUserById(@Param("id") String id);

    Optional<User> findUserByUsername(String username);

}
