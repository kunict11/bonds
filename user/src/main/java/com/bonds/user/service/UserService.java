package com.bonds.user.service;

import com.bonds.user.dto.NewUserRequestDTO;
import com.bonds.user.model.User;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface UserService {
    List<User> listAllUsers();

    List<User> listAllFollowersForUserById(String id);

    User getUserById(String id);

    User getUserByUsername(String username);

    String addUser(NewUserRequestDTO user, MultipartFile image);

    void followUser(String userId, String otherId);

    void unfollowUser(String userId, String otherId);

   // for testing/debugging
    void deleteAll();
}
