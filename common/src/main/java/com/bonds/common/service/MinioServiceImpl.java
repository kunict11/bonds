package com.bonds.common.service;

import io.minio.BucketExistsArgs;
import io.minio.GetObjectArgs;
import io.minio.MakeBucketArgs;
import io.minio.MinioClient;
import io.minio.PutObjectArgs;
import io.minio.RemoveObjectArgs;
import io.minio.UploadObjectArgs;
import io.minio.errors.MinioException;
import org.apache.commons.compress.utils.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@Service
public class MinioServiceImpl implements MinioService {

    private static final Logger logger = LoggerFactory.getLogger(MinioServiceImpl.class);

    @Value("${minio.bucket}")
    private String bucketName;
    private final MinioClient minioClient;

    public MinioServiceImpl(MinioClient minioClient) {
        this.minioClient = minioClient;
    }

    @Override
    public void uploadPhoto(String objectName, MultipartFile image)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        try {
            createBucketIfNotExists(bucketName);
            minioClient.putObject(PutObjectArgs
                    .builder()
                    .bucket(bucketName)
                    .object(objectName)
                    .contentType(image.getContentType())
                    .stream(image.getInputStream(), image.getSize(), -1)
                    .build());
        } catch (MinioException me) {
            logger.error("Minio error when uploading photo " + image.getOriginalFilename(), me);
        }
    }

    @Override
    public byte[] getPhoto(String imagePath)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException {

        try (InputStream objStream = minioClient.getObject(
                GetObjectArgs.builder()
                    .bucket(bucketName)
                    .object(imagePath)
                    .build()
                )
            ) {
            return IOUtils.toByteArray(objStream);
        } catch (MinioException me) {
            logger.error("Minio error when fetching photo " + imagePath, me);
            return null;
        }

    }

    @Override
    public void deletePhoto(String imagePath)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException {
        try {
            minioClient.removeObject(RemoveObjectArgs
                    .builder()
                    .bucket(bucketName)
                    .object(imagePath)
                    .build()
            );
        } catch (MinioException ex) {
            logger.error("Minio error while trying to delete photo " + imagePath, ex);
        }

    }

    private void createBucketIfNotExists(String bucketName)
            throws InvalidKeyException, NoSuchAlgorithmException, MinioException, IOException {
        boolean exists = minioClient.bucketExists(BucketExistsArgs
                .builder()
                .bucket(bucketName)
                .build());
        if (!exists) {
            logger.info("Creating new bucket: {}", bucketName);
            minioClient.makeBucket(MakeBucketArgs
                    .builder()
                    .bucket(bucketName)
                    .build());
        } else
            logger.debug("Bucket with name " + bucketName + " exists. Using bucket " + bucketName);
    }
}
