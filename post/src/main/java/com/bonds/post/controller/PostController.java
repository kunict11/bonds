package com.bonds.post.controller;

import com.bonds.common.util.JwtUtil;
import com.bonds.post.dto.PostRequestDTO;
import com.bonds.post.dto.PostResponseDTO;
import com.bonds.post.model.Comment;
import com.bonds.post.model.Post;
import com.bonds.post.service.PostService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/posts")
public class PostController {

    private static final Logger logger = LoggerFactory.getLogger(PostController.class);
    public final PostService postService;

    PostController(PostService postService) {
        this.postService = postService;
    }

    @GetMapping(value = "/", produces = "application/json")
    public ResponseEntity<List<Post>> getAllPosts() {
        List<Post> posts = postService.getAllPosts();

        return new ResponseEntity<>(posts, HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<PostResponseDTO> getPostById(@PathVariable long id) {
        PostResponseDTO post = postService.getPostById(id);

        return new ResponseEntity<>(post, HttpStatus.OK);
    }

    @PostMapping(value = "/", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Post> submitPost(
            @RequestPart("postDTO") PostRequestDTO postDTO,
            @RequestPart(value = "image", required = false) MultipartFile image) {
        Post createdPost = postService.createPost(postDTO, image);

        return new ResponseEntity<>(createdPost, HttpStatus.CREATED);
    }

    @PutMapping("/like/{postId}")
    public void likePost(@PathVariable long postId, @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        String token = bearerToken.split(" ")[1];
        String userId = JwtUtil.extractUserId(token);

        postService.likePost(postId, userId);
    }

    @GetMapping("/comment/{postId}")
    public ResponseEntity<List<Comment>> getCommentsForPost(@PathVariable long postId) {
        List<Comment> comments = postService.getAllCommentsForPost(postId);

        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @PutMapping("/comment/{postId}/{userId}")
    public void addComment(@RequestBody Comment comment, @PathVariable long postId, @PathVariable String userId) {
        postService.postComment(postId, comment, userId);
    }

    @DeleteMapping("/{id}")
    public void deletePost(@PathVariable long id) {
        postService.deletePostById(id);
    }
}
