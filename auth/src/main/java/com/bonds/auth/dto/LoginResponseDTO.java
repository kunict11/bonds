package com.bonds.auth.dto;

public record LoginResponseDTO (
    String username,
    String token,
    //TODO: add refresh token
    long expiresIn
) { }
