package com.bonds.user.controller;

import com.bonds.common.util.JwtUtil;
import com.bonds.user.dto.NewUserRequestDTO;
import com.bonds.user.model.User;
import com.bonds.user.service.UserService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    UserService userService;

    UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "/", produces = "application/json")
    public ResponseEntity<List<User>> getAllUsers() {
        return new ResponseEntity<>(userService.listAllUsers(), HttpStatus.OK);
    }

    @GetMapping(value = "/followers/{id}", produces = "application/json")
    public ResponseEntity<List<User>> getAllFollowersForUser(@PathVariable String id) {
        return new ResponseEntity<>(userService.listAllFollowersForUserById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/{id}", produces = "application/json")
    public ResponseEntity<User> getUserById(@PathVariable String id) {
        return new ResponseEntity<>(userService.getUserById(id), HttpStatus.OK);
    }

    @GetMapping(value = "/{username}", produces = "application/json")
    public ResponseEntity<User> getUserByUsername(@PathVariable String username) {
        return new ResponseEntity<>(userService.getUserByUsername(username), HttpStatus.OK);
    }

    @PostMapping(value = "/", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<String> addNewUser(
            @RequestPart("newUserDTO") NewUserRequestDTO user,
            @RequestPart(value = "profilePicture", required = false) MultipartFile image) {
        String userId = userService.addUser(user, image);
        return new ResponseEntity<>(userId, HttpStatus.CREATED);
    }

    @PutMapping(value = "/follow/{userId}")
    public void followUser(@PathVariable String userId, @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        String token = bearerToken.split(" ")[1];
        String currentUserId = JwtUtil.extractUserId(token);

        userService.followUser(userId, currentUserId);
    }

    @PutMapping(value = "/unfollow/{userId}")
    public void unfollowUser(@PathVariable String userId, @RequestHeader(HttpHeaders.AUTHORIZATION) String bearerToken) {
        String token = bearerToken.split(" ")[1];
        String currentUserId = JwtUtil.extractUserId(token);

        userService.unfollowUser(userId, currentUserId);
    }


    // for testing/debugging
    @DeleteMapping(value = "/")
    public void deleteAll() {
        userService.deleteAll();
    }
}
