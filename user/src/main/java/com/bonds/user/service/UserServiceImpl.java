package com.bonds.user.service;

import com.bonds.common.service.MinioService;
import com.bonds.user.dto.NewUserRequestDTO;
import com.bonds.user.exception.UserNotFoundException;
import com.bonds.user.model.Notification;
import com.bonds.user.model.User;
import com.bonds.user.model.UserGender;
import com.bonds.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.bonds.user.config.KafkaConfig.USER_FOLLOWED_TOPIC_NAME;

@Service
public class UserServiceImpl implements UserService {
    Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    private final MinioService minioService;
    private final UserRepository userRepository;
    private final KafkaTemplate<String, Notification> kafkaTemplate;

    UserServiceImpl(MinioService minioService, UserRepository userRepository, KafkaTemplate<String, Notification> kafkaTemplate) {
        this.minioService = minioService;
        this.userRepository = userRepository;
        this.kafkaTemplate = kafkaTemplate;
    }

    @Override
    public List<User> listAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<User> listAllFollowersForUserById(String id) {
        return userRepository.findAllFollowersForUserById(id);
    }

    @Override
    public User getUserById(String id) {
        Optional<User> optUser = userRepository.findById(id);

        if(optUser.isEmpty())
            throw new UserNotFoundException("User with id " + id + " does not exist.");

        return optUser.get();
    }

    @Override
    public User getUserByUsername(String username) {

        Optional<User> optUser = userRepository.findUserByUsername(username);

        if(optUser.isEmpty())
            throw new UserNotFoundException("User with username " + username + " does not exist.");

        return optUser.get();
    }

    @Override
    public String addUser(NewUserRequestDTO userRequestDTO, MultipartFile image) {
        String photoPath = "";
        if(image != null) {
            photoPath = uploadPhoto(userRequestDTO.username(), image);
        }
        User newUser = new User();
        newUser.setUsername(userRequestDTO.username());
        newUser.setAlias(userRequestDTO.alias());
        newUser.setBio(userRequestDTO.bio());
        newUser.setPictureUrl(photoPath);
        newUser.setGender(UserGender.valueOf(userRequestDTO.gender().toUpperCase()));
        newUser.setCountry(userRequestDTO.country());
        User user = userRepository.save(newUser);

        return user.getId();
    }

    @Override
    public void followUser(String userId, String currentUserId) {
        Optional<User> optUser = userRepository.findById(userId);
        Optional<User> optCurrentUser = userRepository.findById(currentUserId);

        if(optUser.isEmpty())
            throw new UserNotFoundException("User with id " + userId + " does not exist.");

        if(optCurrentUser.isEmpty())
            throw new UserNotFoundException("User with id " + currentUserId + " does not exist.");

        User user = optUser.get();
        User currentUser = optCurrentUser.get();

        currentUser.followUser(user);
        
        String notificationMessage = currentUser.getUsername() + " followed you.";
        sendNotification(USER_FOLLOWED_TOPIC_NAME, currentUser.getId(), user.getId(), notificationMessage);
        userRepository.save(currentUser);
    }

    @Override
    public void unfollowUser(String userId, String currentUserId) {
        Optional<User> optUser = userRepository.findById(userId);
        Optional<User> optCurrentUser = userRepository.findById(currentUserId);

        if(optUser.isEmpty())
            throw new UserNotFoundException("User with id " + userId + " does not exist.");

        if(optCurrentUser.isEmpty())
            throw new UserNotFoundException("User with id " + currentUserId + " does not exist.");

        User user = optUser.get();
        User currentUser = optCurrentUser.get();

        currentUser.unfollowUser(user);

        userRepository.save(currentUser);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    private void sendNotification(String topicName, String fromUserId, String toUserId, String msg) {
        Notification notification = new Notification(fromUserId, toUserId, msg, "follow");
        kafkaTemplate.send(topicName, notification);
    }

    private String uploadPhoto(String userId, MultipartFile image) {
        String objectName = userId + "/" + UUID.randomUUID() + image.getOriginalFilename();
        try {
            minioService.uploadPhoto(objectName, image);
            return objectName;
        } catch (Exception e) {
            logger.error("An error occurred when uploading profile photo {}: ", image.getOriginalFilename() , e);
            return "";
        }
    }
}
