package com.bonds.auth.filter;

import com.bonds.auth.model.UserAuth;
import com.bonds.auth.repository.UserAuthRepository;
import com.bonds.common.util.JwtUtil;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Arrays;

import static com.bonds.auth.config.SecurityConfig.PERMITTED_PATHS;

@Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    Logger logger = LoggerFactory.getLogger(JwtAuthenticationFilter.class);

    private final UserAuthRepository userAuthRepository;

    public JwtAuthenticationFilter(UserAuthRepository userAuthRepository) {
        this.userAuthRepository = userAuthRepository;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain filterChain)
            throws ServletException, IOException {

        if(Arrays.stream(PERMITTED_PATHS).anyMatch(permittedPath -> new AntPathMatcher().match(permittedPath, request.getRequestURI()))) {
            filterChain.doFilter(request, response);
            return;
        }
        String authHeader = request.getHeader("Authorization");
        // the authorization header must be part of the request
        // the valid format for the header value is 'Bearer <token>'
        if (authHeader == null || !authHeader.startsWith("Bearer ")) {
            logger.error("Invalid authorization header format. Value of auth header {} for request id {}", request.getHeader("Authorization"), request.getRequestId());
            filterChain.doFilter(request, response);
            return;
        }

        String token = authHeader.split(" ")[1];

        String username = JwtUtil.extractUsername(token);
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        if (username != null && authentication == null) {
            UserAuth userAuth = userAuthRepository
                    .findUserAuthByUsername(username)
                    .orElseThrow(() -> new UsernameNotFoundException("User with username " + " not found."));

            if (JwtUtil.isTokenValid(token, userAuth)) {
                UsernamePasswordAuthenticationToken authToken =
                        new UsernamePasswordAuthenticationToken(userAuth, null, userAuth.getAuthorities());
                authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(authToken);
            } else
                logger.error("Invalid Jwt token for request id {}", request.getRequestId());
        }

        filterChain.doFilter(request, response);
    }
}
