package com.bonds.post.service;

import com.bonds.common.service.MinioService;
import com.bonds.post.dto.PostRequestDTO;
import com.bonds.post.dto.PostResponseDTO;
import com.bonds.post.exception.PostInternalException;
import com.bonds.post.exception.PostNotFoundException;
import com.bonds.post.model.Comment;
import com.bonds.post.model.Notification;
import com.bonds.post.model.Post;
import com.bonds.post.model.PostNotificationType;
import com.bonds.post.repository.PostRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.bonds.post.config.KafkaProducerConfig.POST_COMMENTS_TOPIC_NAME;
import static com.bonds.post.config.KafkaProducerConfig.POST_LIKES_TOPIC_NAME;

@Service
public class PostServiceImpl implements PostService {
    Logger logger = LoggerFactory.getLogger(PostServiceImpl.class);

    private final MinioService minioService;
    private final PostRepository postRepository;
    private final KafkaTemplate<String, Notification> kafkaTemplate;

    PostServiceImpl(PostRepository postRepository, MinioService minioService, KafkaTemplate<String, Notification> kafkaTemplate) {
        this.postRepository = postRepository;
        this.minioService = minioService;
        this.kafkaTemplate = kafkaTemplate;
    }


    @Override
    public List<Post> getAllPosts() {
        return postRepository.findAll();
    }

    @Override
    public PostResponseDTO getPostById(long id) {
        Optional<Post> postOpt = postRepository.findById(id);

        if (postOpt.isEmpty())
            throw new PostNotFoundException("Post with given id: " + id + " doesn't exist.");

        Post post = postOpt.get();
        byte[] photoBytes = getPhotoBytesForPost(post);

        return new PostResponseDTO (
                post.getUserId(),
                post.getText(),
                photoBytes,
                post.getLikes(),
                post.getComments()
        );
    }

    @Override
    public Post createPost(PostRequestDTO postDTO, MultipartFile image) {
        String imageName = "";
        if (image != null)
            imageName = uploadPhoto(postDTO.userId(), image);

        Post post = new Post();
        post.setUserId(postDTO.userId());
        post.setText(postDTO.text());
        post.setImageName(imageName);
        post.setTimestamp(System.currentTimeMillis());

        return postRepository.save(post);
    }

    @Override
    // TODO: Get id of logged in user
    public void likePost(long postId, String userId) {
        Optional<Post> postOpt = postRepository.findById(postId);

        if(postOpt.isEmpty())
            throw new PostNotFoundException("Post with id "+ postId +" does not exist");

        Post post = postOpt.get();
        post.addLike(userId);

        postRepository.save(post);

        String notificationMessage = "User " + userId + " liked your post " + postId;
        sendNotification(POST_LIKES_TOPIC_NAME, userId, post.getUserId(), postId, notificationMessage, PostNotificationType.LIKE);
    }

    @Override
    public void deletePostById(long id) {
        Optional<Post> postOpt = postRepository.findById(id);

        if (postOpt.isEmpty())
            throw new PostNotFoundException("Post with given id: " + id + " doesn't exist.");

        Post post = postOpt.get();
        if(!post.getImageName().isEmpty()) {
            try {
                minioService.deletePhoto(post.getImageName());
            } catch (Exception ex) {
                throw new PostInternalException("An internal server error occurred while trying to delete photo", ex);
            }
        }

        postRepository.delete(post);
    }

    @Override
    public List<Comment> getAllCommentsForPost(long postId) {
        Optional<Post> postOpt = postRepository.findById(postId);

        if (postOpt.isEmpty())
            throw new PostNotFoundException("Post with given id: " + postId + " doesn't exist.");

        return postOpt.get().getComments();
    }

    @Override
    // TODO: pass userId from auth token
    public void postComment(long postId, Comment comment, String userId) {
        Optional<Post> postOpt = postRepository.findById(postId);

        if (postOpt.isEmpty())
            throw new PostNotFoundException("Post with given id: " + postId + " doesn't exist.");

        Post post = postOpt.get();
        comment.setTimestamp(System.currentTimeMillis());
        post.addComment(comment);

        postRepository.save(post);

        String notificationMessage = "User " + userId + " commented on your post " + comment;
        sendNotification(POST_COMMENTS_TOPIC_NAME, userId, post.getUserId(), postId, notificationMessage, PostNotificationType.COMMENT);
    }

    private void sendNotification(String topicName, String fromUserId, String toUserId, Long postId, String msg, PostNotificationType type) {
        Notification notification = new Notification(fromUserId, toUserId, postId, msg, type);
        kafkaTemplate.send(topicName, notification);
    }

    private String uploadPhoto(String userId, MultipartFile image) {
        String objectName = userId + "/" + UUID.randomUUID() + image.getOriginalFilename();
        try {
            minioService.uploadPhoto(objectName, image);
            return objectName;
        } catch (Exception e) {
            logger.error("An error occurred when uploading photo " + image.getOriginalFilename(), e);
            return "";
        }
    }

    private byte[] getPhotoBytesForPost(Post post) {
        byte[] photoBytes = new byte[]{};

        String imageName = post.getImageName();
        if(!imageName.isEmpty()) {
            try {
                photoBytes = minioService.getPhoto(imageName);
            } catch (Exception e) {
                logger.error("Couldn't fetch photo from MinIO: {}", imageName, e);
                throw new PostInternalException("The server encountered an error and could not complete your request.", e);
            }
        }

        return photoBytes;
    }
}
