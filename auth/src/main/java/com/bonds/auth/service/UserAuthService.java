package com.bonds.auth.service;

import com.bonds.auth.dto.LoginRequestDTO;
import com.bonds.auth.dto.RegisterRequestDTO;
import com.bonds.auth.model.UserAuth;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserAuthService extends UserDetailsService {

    UserAuth loadUserByUsername(String username);
    UserAuth registerNewUser(RegisterRequestDTO registerRequestDTO, MultipartFile profilePicture);
    void deleteUserAuthByUsername(String username);
}
