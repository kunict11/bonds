-- DROP TABLE IF EXISTS user_auth;

CREATE TABLE IF NOT EXISTS user_auth
(
    id       SERIAL PRIMARY KEY,
    username TEXT NOT NULL,
    email    TEXT NOT NULL ,
    password TEXT NOT NULL,
    role     TEXT
);