package com.bonds.notifications.kafka;

import com.bonds.notifications.model.Notification;
import com.bonds.notifications.repository.NotificationRepository;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

@Service
public class NotificationListener {
    private final Sinks.Many<Notification> sink = Sinks.many().multicast().onBackpressureBuffer();
    private final Sinks.Many<String> stringSink = Sinks.many().multicast().onBackpressureBuffer();
    private final NotificationRepository notificationRepository;

    NotificationListener(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @KafkaListener(topics = {"post-likes", "post-comments", "user-followed"})
    public void listen(Notification notification) {
        notification.setRead(false);
        notificationRepository.save(notification).subscribe();

        sink.tryEmitNext(notification);
    }

    public Flux<Notification> getNotifications() {
        return sink.asFlux();
    }

    public Flux<String> getStrNotifications() {
        return stringSink.asFlux();
    }
}
