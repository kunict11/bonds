package com.bonds.post.exception;

import com.bonds.common.exception.RestErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class PostExceptionHandler {
    Logger log = LoggerFactory.getLogger(PostExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handlePostNotFoundException(PostNotFoundException ex) {
        RestErrorResponse errorResponse = new RestErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                ex.getMessage(),
                System.currentTimeMillis()
        );

        log.error("Error {}, encountered exception: {}.", HttpStatus.NOT_FOUND, ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handlePostInternalException(PostInternalException ex) {
        RestErrorResponse errorResponse = new RestErrorResponse(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                ex.getMessage(),
                System.currentTimeMillis()
        );

        log.error("Error {}, encountered exception: {}.", HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage(), ex);
        return new ResponseEntity<>(errorResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
