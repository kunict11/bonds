package com.bonds.notifications.model;

import com.mongodb.lang.Nullable;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collation = "notifications")
public class Notification {

    @Id
    ObjectId id;
    String fromUserId;
    @Indexed
    String toUserId;
    @Nullable
    Long postId;
    String message;
    NotificationType type;
    boolean read;


    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getFromUserId() {
        return fromUserId;
    }

    public void setFromUserId(String fromUserId) {
        this.fromUserId = fromUserId;
    }

    public String getToUserId() {
        return toUserId;
    }

    @Nullable
    public Long getPostId() {
        return postId;
    }

    public void setPostId(@Nullable Long postId) {
        this.postId = postId;
    }

    public void setToUserId(String toUserId) {
        this.toUserId = toUserId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Date getDateFromObjectId() {
        return this.id.getDate();
    }

    @Override
    public String toString() {
        return "Notification [id=" + id +
                ", fromUserId=" + fromUserId + ", toUserId=" + toUserId +
                ", type=" + type + ", read=" + read + "]";
    }
}
