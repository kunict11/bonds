package com.bonds.auth.repository;

import com.bonds.auth.model.UserAuth;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserAuthRepository extends CrudRepository<UserAuth, Long> {

    Optional<UserAuth> findUserAuthByUsername(String username);
    boolean existsUserAuthByUsername(String username);
}
