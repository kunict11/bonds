package com.bonds.post.dto;

import com.bonds.post.model.Comment;

import java.util.List;

public record PostResponseDTO(String userId, String text, byte[] photo, List<String> likes, List<Comment> comments) { }
