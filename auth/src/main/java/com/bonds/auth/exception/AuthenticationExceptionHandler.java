package com.bonds.auth.exception;

import com.bonds.common.exception.RestErrorResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AuthenticationExceptionHandler {
    Logger logger = LoggerFactory.getLogger(AuthenticationExceptionHandler.class);

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleUsernameNotFoundException(UsernameNotFoundException e) {
        RestErrorResponse response = new RestErrorResponse(
                HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                System.currentTimeMillis()
        );

        logger.error("Error processing request: {}. Encountered exception: {}", HttpStatus.NOT_FOUND, e.getMessage(), e);
        return new ResponseEntity<>(response, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleUsernameAlreadyTakenException(UsernameAlreadyTakenException e) {
        RestErrorResponse response = new RestErrorResponse(
                HttpStatus.CONFLICT.value(),
                e.getMessage(),
                System.currentTimeMillis()
        );

        logger.error("Error processing request: {}. Encountered exception: {}", HttpStatus.CONFLICT, e.getMessage(), e);
        return new ResponseEntity<>(response, HttpStatus.CONFLICT);
    }

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleBadCredentialsException(BadCredentialsException e) {
        RestErrorResponse response = new RestErrorResponse(
                HttpStatus.FORBIDDEN.value(),
                e.getMessage(),
                System.currentTimeMillis()
        );

        logger.error("Error processing request: {}. Encountered exception: {}", HttpStatus.FORBIDDEN, e.getMessage(), e);
        return new ResponseEntity<>(response, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler
    public ResponseEntity<RestErrorResponse> handleInternalServerError(InternalServerErrorException e) {
        RestErrorResponse response = new RestErrorResponse(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                e.getMessage(),
                System.currentTimeMillis()
        );

        logger.error("Error processing request: {}. Encountered exception: {}", HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), e);
        return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
