package com.bonds.common.util;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.userdetails.UserDetails;

import javax.crypto.SecretKey;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;

public final class JwtUtil {

    // TODO: create a file with secrets
    private static final String SECRET_KEY = "hoOmFolkxw9+etUDA4wBDyslnLvKWL3HdFI89Xxaib8=";
    public static final long TOKEN_DURATION = 900000L;

    private JwtUtil() { }
    public static String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    public static String extractUserId(String token) {
        return extractClaim(token, claims -> claims.get("user-id", String.class));
    }

    public static <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        Claims claims = extractAllClaims(token);
        return claimsResolver.apply(claims);
    }

    public static boolean isTokenValid(String token) {
        try {
            Claims claims = extractAllClaims(token);

            return claims != null && !isTokenExpired(token);
        } catch (IllegalArgumentException | JwtException e) {
            return false;
        }
    }

    public static boolean isTokenValid(String token, UserDetails userDetails) {
        String username = extractUsername(token);
        return username.equals(userDetails.getUsername()) && !isTokenExpired(token);
    }

    public static SecretKey getSecretKey() {
        byte[] decodedKey = Base64.getDecoder().decode(SECRET_KEY);
//        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "PBKDF2WithHmacSHA256");
        return Keys.hmacShaKeyFor(decodedKey);
    }

    private static boolean isTokenExpired(String token) {
        return getExpiration(token).before(new Date());
    }

    private static Date getExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    private static Claims extractAllClaims(String token) {
        return Jwts
                .parser()
                .verifyWith(getSecretKey())
                .build()
                .parseSignedClaims(token)
                .getPayload();

    }
}
