package com.bonds.post.exception;

public class PostInternalException extends RuntimeException {
    public PostInternalException(String message, Throwable cause) {
        super(message, cause);
    }

    public PostInternalException(String message) {
        super(message);
    }

    public PostInternalException(Throwable cause) {
        super(cause);
    }
}
