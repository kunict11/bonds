echo "Creating kind cluster..."
cat <<EOF | kind create cluster --config=-
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: bonds-dev
nodes:
- role: control-plane
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 80
    protocol: TCP
  - containerPort: 443
    hostPort: 443
    protocol: TCP
EOF
echo "Cluster 'bonds-dev' created."

# setup kind specific patches to forward the hostPorts to the ingress controller
# https://kind.sigs.k8s.io/docs/user/ingress/#ingress-nginx
echo "Setting up Ingress..."
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml

# wait until ready to process requests
# https://kind.sigs.k8s.io/docs/user/ingress/#ingress-nginx
kubectl wait --namespace ingress-nginx \
  --for=condition=ready pod \
  --selector=app.kubernetes.io/component=controller \
  --timeout=90s
echo "Ingress setup complete."

# build Common package
docker build -t bonds/common-build:latest ../../common

echo "Building services..."
echo "Building Discovery Server"
docker build -t bonds/discovery-server:latest ../../discovery-server

echo "Building API Gateway"
docker build -t bonds/gateway-service:latest ../../gateway

echo "Building Posts service"
docker build -t bonds/post-service:latest ../../post

echo "Building Notifications service"
docker build -t bonds/notifications-service:latest ../../notifications

echo "Loading docker images to kind cluster..."
kind load docker-image bonds/discovery-server:latest bonds/discovery-server:latest --name bonds-dev
kind load docker-image bonds/gateway-service:latest bonds/gateway-service:latest --name bonds-dev
kind load docker-image bonds/post-service:latest bonds/post-service:latest --name bonds-dev
kind load docker-image bonds/notifications-service:latest bonds/notifications-service:latest --name bonds-dev
echo "Done."

echo "Applying Kubernetes deployments..."
kubectl apply -f ../kube
echo "Done deploying pods."
echo "Start using the app by going to http://bonds.localhost/api/<api_name>"