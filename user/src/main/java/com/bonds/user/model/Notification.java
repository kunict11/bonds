package com.bonds.user.model;

public record Notification(
        String fromUserId,
        String toUserId,
        String message,
        String type
) {
}
