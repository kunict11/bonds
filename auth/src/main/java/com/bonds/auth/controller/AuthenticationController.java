package com.bonds.auth.controller;

import com.bonds.auth.dto.LoginRequestDTO;
import com.bonds.auth.dto.LoginResponseDTO;
import com.bonds.auth.dto.RegisterRequestDTO;
import com.bonds.auth.model.UserAuth;
import com.bonds.auth.service.JwtTokenProvider;
import com.bonds.auth.service.UserAuthService;
import com.bonds.common.util.JwtUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/auth")
public class AuthenticationController {
    private final UserAuthService userAuthService;
    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;

    public AuthenticationController(UserAuthService userAuthService, JwtTokenProvider jwtTokenProvider, AuthenticationManager authenticationManager) {
        this.userAuthService = userAuthService;
        this.jwtTokenProvider = jwtTokenProvider;
        this.authenticationManager = authenticationManager;
    }

    @PostMapping(value = "/register", consumes = {MediaType.MULTIPART_FORM_DATA_VALUE}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<LoginResponseDTO> registerNewUser(
            @RequestPart("registrationData") RegisterRequestDTO registerRequestDTO,
            @RequestPart(name = "profilePicture", required = false) MultipartFile profilePicture) {
        UserAuth userAuth = userAuthService.registerNewUser(registerRequestDTO, profilePicture);
        String token = jwtTokenProvider.generateToken(userAuth);

        return new ResponseEntity<>(new LoginResponseDTO(userAuth.getUsername(), token, JwtUtil.TOKEN_DURATION), HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<LoginResponseDTO> login(@RequestBody LoginRequestDTO loginRequestDTO) {
        authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequestDTO.username(), loginRequestDTO.password()));

        UserAuth userAuth =  userAuthService.loadUserByUsername(loginRequestDTO.username());
        String token = jwtTokenProvider.generateToken(userAuth);

        return new ResponseEntity<>(new LoginResponseDTO(userAuth.getUsername(), token, JwtUtil.TOKEN_DURATION), HttpStatus.OK);
    }
}
