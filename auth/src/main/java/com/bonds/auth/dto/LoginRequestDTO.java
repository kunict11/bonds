package com.bonds.auth.dto;

public record LoginRequestDTO (
        String username,
        String password
) { }
