package com.bonds.common.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public interface MinioService {
    void uploadPhoto(String objectName, MultipartFile image)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException;

    byte[] getPhoto(String imagePath)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException;

    void deletePhoto(String imagePath)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException;
}

