package com.bonds.common.exception;

public record RestErrorResponse(int code, String message, long timestamp) { }
