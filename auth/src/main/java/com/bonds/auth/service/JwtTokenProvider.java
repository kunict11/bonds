package com.bonds.auth.service;

import com.bonds.auth.model.UserAuth;
import com.bonds.common.util.JwtUtil;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class JwtTokenProvider {

    public String generateToken(UserDetails userDetails) {
        return generateToken(Map.of("user-id", ((UserAuth)userDetails).getUserId()), userDetails);
    }

    public String generateToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return buildToken(extraClaims, userDetails);
    }

    private String buildToken(Map<String, Object> extraClaims, UserDetails userDetails) {
        return Jwts
                .builder()
                .claims()
                .subject(userDetails.getUsername())
                .issuedAt(new Date(System.currentTimeMillis()))
                .expiration(new Date(System.currentTimeMillis() + JwtUtil.TOKEN_DURATION))
                .add(extraClaims)
                .and()
                .signWith(JwtUtil.getSecretKey())
                .compact();
    }
}
