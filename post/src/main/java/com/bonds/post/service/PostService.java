package com.bonds.post.service;

import com.bonds.post.dto.PostRequestDTO;
import com.bonds.post.dto.PostResponseDTO;
import com.bonds.post.model.Comment;
import com.bonds.post.model.Post;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface PostService {

    List<Post> getAllPosts();

    PostResponseDTO getPostById(long id);

    Post createPost(PostRequestDTO postDTO, MultipartFile image);

    void likePost(long postId, String userId);

    List<Comment> getAllCommentsForPost(long postId);

    void postComment(long postId, Comment comment, String userId);

    void deletePostById(long id);

}
