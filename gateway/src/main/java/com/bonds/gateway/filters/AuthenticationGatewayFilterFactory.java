package com.bonds.gateway.filters;

import com.bonds.common.util.JwtUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationGatewayFilterFactory extends AbstractGatewayFilterFactory<AuthenticationGatewayFilterFactory.Config> {
    Logger logger = LoggerFactory.getLogger(AuthenticationGatewayFilterFactory.class);

    AuthenticationGatewayFilterFactory() {
        super(Config.class);
    }
    @Override
    public GatewayFilter apply(Config config) {
        return (exchange, chain) -> {
            String authHeader = exchange.getRequest().getHeaders().getFirst("Authorization");

            if (authHeader == null || !authHeader.startsWith("Bearer ")) {
                var response = exchange.getResponse();

                response.setStatusCode(HttpStatus.UNAUTHORIZED);
                return response.setComplete();
            }
            String token = authHeader.split(" ")[1];
            if(!JwtUtil.isTokenValid(token)) {
                var response = exchange.getResponse();

                response.setStatusCode(HttpStatus.UNAUTHORIZED);

                logger.error("Invalid Jwt token for request id {}. Path {}", exchange.getRequest().getId(), exchange.getRequest().getPath());
                return response.setComplete();
            }
            return chain.filter(exchange);
        };
    }

    static class Config {

    }

}
