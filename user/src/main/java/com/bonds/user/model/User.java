package com.bonds.user.model;

import org.springframework.data.neo4j.core.schema.GeneratedValue;
import org.springframework.data.neo4j.core.schema.Id;
import org.springframework.data.neo4j.core.schema.Node;
import org.springframework.data.neo4j.core.schema.Relationship;
import org.springframework.data.neo4j.core.support.UUIDStringGenerator;

import java.util.HashSet;
import java.util.Set;

@Node
public class User {

    @Id
    @GeneratedValue(UUIDStringGenerator.class)
    private String id;

    private String username;
    private String pictureUrl;
    private String alias;
    private String bio;
    private UserGender gender;
    private String country;

    @Relationship(type = "FOLLOWING", direction = Relationship.Direction.OUTGOING)
    private Set<User> following;

    public String getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public Set<User> getFollowing() {
        if (following == null)
            following = new HashSet<>();
        return following;
    }

    public void followUser(User user) {
        if(following == null)
            following = new HashSet<>();
        following.add(user);
    }
    public void unfollowUser(User user) {
        if(following == null)
            return;
        following.remove(user);
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getBio() {
        return bio;
    }

    public void setBio(String bio) {
        this.bio = bio;
    }

    public UserGender getGender() {
        return gender;
    }

    public void setGender(UserGender gender) {
        this.gender = gender;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
