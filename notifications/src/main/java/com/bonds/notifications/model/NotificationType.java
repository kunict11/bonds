package com.bonds.notifications.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum NotificationType {
    @JsonProperty("like")
    LIKE("like"),

    @JsonProperty("comment")
    COMMENT("comment"),

    @JsonProperty("follow")
    FOLLOW("follow");

    private final String label;

    NotificationType(String label) {
        this.label = label;
    }
}
