drop table if exists post_likes;
drop table if exists comment;
drop table if exists post;

create table post
(
    id        serial primary key,
    user_id   bigint,
    text      text,
    image_url text,
    likes     text[]
);
create table comment
(
    id      serial primary key,
    user_id bigint,
    text    text,
    post_id bigint
)