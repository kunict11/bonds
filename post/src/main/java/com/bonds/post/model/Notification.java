package com.bonds.post.model;

public record Notification (
        String fromUserId,
        String toUserId,
        Long postId,
        String message,
        PostNotificationType type

) { }
