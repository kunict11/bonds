package com.bonds.auth.dto;

import org.springframework.web.multipart.MultipartFile;

public record NewUserRequestDTO (
        String username,
        String alias,
        String bio,
        String gender,
        String country
) { }
