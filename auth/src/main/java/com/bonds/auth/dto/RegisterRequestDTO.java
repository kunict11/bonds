package com.bonds.auth.dto;

public record RegisterRequestDTO (
        String email,
        String username,
        String password,
        String alias,
        String bio,
        String gender,
        String country
) { };

