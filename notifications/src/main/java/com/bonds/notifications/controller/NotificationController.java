package com.bonds.notifications.controller;

import com.bonds.notifications.kafka.NotificationListener;
import com.bonds.notifications.model.Notification;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.util.UUID;

@RestController
@RequestMapping("/api/notifications")
public class NotificationController {
    private final NotificationListener notificationListener;

    NotificationController(NotificationListener notificationListener) {
        this.notificationListener = notificationListener;
    }

    @GetMapping("/")
    public Flux<ServerSentEvent<Notification>> emitNotifications() {
        return notificationListener.getNotifications()
                .map(notification -> ServerSentEvent
                        .<Notification>builder()
                        .id(notification.getId().toString())
                        .event(notification.getType().name())
                        .data(notification)
                        .build()
                );
    }

    @GetMapping("/{userId}")
    public Flux<ServerSentEvent<Notification>> emitNotificationsToUser(@PathVariable("userId") String userId) {
        return notificationListener.getNotifications()
                .filter(notification -> notification.getToUserId().equals(userId))
                .map(notification -> ServerSentEvent
                        .<Notification>builder()
                        .id(notification.getId().toString())
                        .event(notification.getType().name())
                        .data(notification)
                        .build()
                );
    }

    @GetMapping("/demo")
    public Flux<ServerSentEvent<String>> emitNotificationStrings() {
        return notificationListener.getStrNotifications()
                .map(notification -> ServerSentEvent
                        .<String>builder()
                        .id(UUID.randomUUID().toString())
                        .data(notification)
                        .build()
                );
    }
}
