package com.bonds.post.dto;

import org.springframework.web.multipart.MultipartFile;

public record PostRequestDTO (String userId, String text) { }
